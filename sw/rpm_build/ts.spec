Name:           ts
Version:        1.0
Release:        1%{?dist}
Summary:        The Task Spooler project allows you to queue up tasks from the shell for batch execution.

License:        GPLv2+
Group:          Applications/System
URL:            http://vicerveza.homeunix.net/~viric/soft/%{name}/
Source0:        http://vicerveza.homeunix.net/~viric/soft/%{name}/%{name}-%{version}.tar.gz

%description
task spooler is a Unix batch system where the tasks spooled run one after the other. The amount of jobs to run at once can be set at any time. Each user in each system has his own job queue. The tasks are run in the correct context (that of enqueue) from any shell/process, and its output/results can be easily watched. It is very useful when you know that your commands depend on a lot of RAM, a lot of disk use, give a lot of output, or for whatever reason it's better not to run them all at the same time, while you want to keep your resources busy for maximum benfit. Its interface allows using it easily in scripts. 


%prep
%autosetup


%build
make %{?_smp_mflags}

%install
make PREFIX=%{buildroot} install


%files
%license COPYING
%doc Changelog OBJECTIVES PORTABILITY PROTOCOL README TRICKS



%changelog
* Sun Feb 12 2017 Filip Matejicek <matejicek.f@gmail.com> - 1.0
- Initial build
