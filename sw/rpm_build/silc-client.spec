Name:           silc-client
Version:        1.1.11
Release:        2%{?dist}
Summary:        Secure Internet Live Conferencing Client

License:        GPLv2 or BSD 
URL:            http://www.silcnet.org/ 
%define swname silc
Source0:        http://downloads.sourceforge.net/project/%{swname}/%{swname}/client/sources/%{name}-%{version}.tar.gz

ExclusiveArch:  x86_64
BuildRequires:  glib2-devel ncurses-devel gcc

%description
SILC provides common chat and conferencing services like private messages, instant messages, channels and groups, and video and audio conferencing. The main difference to other protocols is that SILC has been designed with security as its main feature. All packets and messages sent in SILC network are always encrypted. Private messages are encrypted end-to-end, channel message are encrypted with channel-wide secret key and secure file transfer is protected peer-to-peer.

%prep
%setup -q

%build
%configure
make %{?_smp_mflags}

%install
make DESTDIR=%{buildroot}
%make_install

%files
%{_sysconfdir}/%{swname}.conf
%{_bindir}/%{swname}
%{_mandir}/man1/%{swname}.1*
%{_datadir}/%{swname}/scripts/*
%{_datadir}/%{swname}/themes/*
%{_datadir}/%{swname}/help/*
%{_docdir}/%{name}/*
%doc README COPYING CREDITS ChangeLog README.PLUGIN
%config %{_sysconfdir}/%{swname}.conf

%changelog
* Tue Mar 08 2016 Filip Matejicek <matejicek.f@gmail.com> - 1.1.11-2
- rebuilt added BuildRequires

* Mon Mar 07 2016 Filip Matejicek <matejicek.f@gmail.com> - 1.1.11
- First release build for amd64/Centos 7
