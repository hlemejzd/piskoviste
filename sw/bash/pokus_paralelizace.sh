#!/usr/bin/env bash

echo $$
sleep 60

open_sem(){
  mkfifo pipe-$$    # vytvor pojmenovanou rouru jmeno pipe-PIDPROCESU
  exec 3<>pipe-$$ # vytvor file descriptor cislo 3 na pojmenovanou rouru pro cteni a zapis
  rm pipe-$$  # smaz pojmenovanou rouru ... sice na disku smazana, ale dokud je fd ## 3 -> '/home/rexxor/pojroura1 (deleted)'
  local i=$1   # pocet opakovani
  for((;i>0;i--)); do   # pro kazde i od vstupu $1 az do 0, udelej $1 = 5 ... 5 4 3 2 1
    printf %s 000 >&3 ### zapis 000 do descriptoru 3
  done
}

# run the given command asynchronously and pop/push tokens
run_with_lock(){
    local x
    # this read waits until there is something to read
    read -u 3 -n 3 x && ((0==x)) || exit $x ## nacti vstup z file descriptoru 3 v poctu 3 znaku do promene x a zaroven zkontroluj, ze jde o 0
    (
     ( "$@"; )  # pust cely command na pozadi
    # push the return code of the command to the semaphore
    printf '%.3d' $? >&3 # V pripade, ze blok skoncil na pozadi, tak pridej exit status do readu zpet, tj pri 0 na 000, pri error 127
    )&
}

N=4
open_sem $N


for thing in {a..g}; do
    run_with_lock task $thing
done

