#!/bin/bash
 
# CHANGELOG
# [SuperPlumus] (2013-06-09 15-11)
#   gettext
#   clean code
#   fix $PLAYONLINUX variable check
#   fix POL_SetupWindow_browe -> POL_SetupWindow_browse (missing 's')
 
[ "$PLAYONLINUX" = "" ] && exit 0
source "$PLAYONLINUX/lib/sources"
 
TITLE="Starcraft"
PREFIX="Starcraft"
WORKING_WINE_VERSION="2.22"
 
POL_SetupWindow_Init
POL_Debug_Init
 
POL_SetupWindow_presentation "$TITLE" "Blizzard" "http://www.blizzard.com" "Quentin PÂRIS" "$PREFIX"
 
POL_Wine_SelectPrefix "$PREFIX"
POL_Wine_PrefixCreate "$WORKING_WINE_VERSION"
 
POL_SetupWindow_InstallMethod "CD,LOCAL"
 
if [ "$INSTALL_METHOD" = "CD" ]; then
    POL_SetupWindow_cdrom
    POL_SetupWindow_check_cdrom "setup.exe"
    SetupIs="$CDROM/setup.exe"
fi
 
if [ "$INSTALL_METHOD" = "LOCAL" ]; then
    POL_SetupWindow_browse "$(eval_gettext 'Please select the setup file to run')" "$TITLE"
    SetupIs="$APP_ANSWER"
fi
 
POL_Wine_WaitBefore "$TITLE"
POL_Wine "$SetupIs"
POL_Wine_WaitExit "$TITLE"
 
POL_Shortcut "Starcraft.exe" "$TITLE"
 
cd "$WINEPREFIX/drive_c/$PROGRAMFILES/Starcraft/"
POL_Download "http://files.playonlinux.com/ddraw.dll" "efe4aa40d8633213dbfd2c927c7c70b0"
 
POL_SetupWindow_Close
exit
